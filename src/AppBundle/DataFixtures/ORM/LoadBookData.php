<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBookData extends Fixture implements DependentFixtureInterface
{

    public const DR_AIBOLIT = 'DR_AIBOLIT';
    public const SUBTLE_ART_OF_POFIGISMA = 'SUBTLE_ART_OF_POFIGISMA';
    public const ORIGIN = 'ORIGIN';
    public const ILLUSION_2 = 'ILLUSION_2';

    public function load(ObjectManager $manager)
    {
        $book = new Book();
        $book
            ->setTitle('Доктор Айболит (сборник)')
            ->setAuthor('Корней Чуковский')
            ->setPreview('5b1bc2dcc68cf.jpg')
            ->addCategories($this->getReference(LoadCategoryData::CHILDREN_LITERATURE))
        ;

        $manager->persist($book);
        $this->addReference(self::DR_AIBOLIT, $book);

        $book1 = new Book();
        $book1
            ->setTitle('Тонкое искусство пофигизма: Парадоксальный способ жить счастливо')
            ->setAuthor('Марк Мэнсон')
            ->setPreview('5b1bc1b368938.jpg')
            ->addCategories($this->getReference(LoadCategoryData::ADVENTURE))
        ;

        $manager->persist($book1);
        $this->addReference(self::SUBTLE_ART_OF_POFIGISMA, $book1);


        $book2 = new Book();
        $book2
            ->setTitle('Происхождение')
            ->setAuthor('Дэн Браун')
            ->setPreview('5b1bc1ff1d615.jpg')
            ->addCategories($this->getReference(LoadCategoryData::DRAMA))
        ;

        $manager->persist($book2);
        $this->addReference(self::ORIGIN, $book2);

        $book3 = new Book();
        $book3
            ->setTitle('Иллюзия 2')
            ->setAuthor('Сергей Тармашев')
            ->setPreview('5b1bc26b30842.jpg')
            ->addCategories($this->getReference(LoadCategoryData::ADVENTURE))
        ;

        $manager->persist($book3);
        $this->addReference(self::ILLUSION_2, $book3);

        $manager->flush();
    }
    public function getDependencies()
    {
        return array(
            LoadCategoryData::class,
        );
    }
}
