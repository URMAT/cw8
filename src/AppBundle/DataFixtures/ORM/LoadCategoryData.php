<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCategoryData extends Fixture
{
    public const ADVENTURE = 'ADVENTURE';
    public const CHILDREN_LITERATURE = 'CHILDREN_LITERATURE';
    public const DRAMA = 'DRAMA';
    public function load(ObjectManager $manager)
    {

        $category = new Category();
        $category
            ->setCategoryTitle('Приключения')
        ;

        $manager->persist($category);
        $this->addReference(self::ADVENTURE, $category);


        $category1 = new Category();
        $category1
            ->setCategoryTitle('Детская литература')
        ;

        $manager->persist($category1);
        $this->addReference(self::CHILDREN_LITERATURE, $category1);

        $category2 = new Category();
        $category2
            ->setCategoryTitle('Драма')
        ;

        $manager->persist($category2);
        $this->addReference(self::DRAMA, $category2);

        $manager->flush();
    }
}
