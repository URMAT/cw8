<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Act
 *
 * @ORM\Table(name="act")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ActRepository")
 */
class Act
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reader_number_ticket", type="string", length=127)
     */
    private $reader_number_ticket;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="datetime")
     */
    private $data;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set readerNumberTicket
     *
     * @param string $readerNumberTicket
     *
     * @return Act
     */
    public function setReaderNumberTicket($readerNumberTicket)
    {
        $this->reader_number_ticket = $readerNumberTicket;

        return $this;
    }

    /**
     * Get readerNumberTicket
     *
     * @return string
     */
    public function getReaderNumberTicket()
    {
        return $this->reader_number_ticket;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Act
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }
}

