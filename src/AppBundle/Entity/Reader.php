<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reader
 *
 * @ORM\Table(name="reader")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReaderRepository")
 */
class Reader
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=127)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=127)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="passport_id", type="string", length=127, nullable=false, unique=true)
     */
    private $passport_id;

    /**
     * @var string
     *
     * @ORM\Column(name="$number_ticket", type="string", length=127, nullable=false, unique=true)
     */
    private $number_ticket;

    /**
     * @var Book
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Book", inversedBy="reader")
     */
    private $book;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Reader
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return Reader
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set passportId
     *
     * @param string $passportId
     *
     * @return Reader
     */
    public function setPassportId($passportId)
    {
        $this->passport_id = $passportId;

        return $this;
    }

    /**
     * Get passportId
     *
     * @return string
     */
    public function getPassportId()
    {
        return $this->passport_id;
    }

    /**
     * @return Book
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * @param Book $book
     */
    public function setBook($book)
    {
        $this->book = $book;
    }

    /**
     * @return string
     */
    public function getNumberTicket()
    {
        return $this->number_ticket;
    }

    /**
     * @param string $number_ticket
     */
    public function setNumberTicket($number_ticket)
    {
        $this->number_ticket = $number_ticket;
    }

    
}

