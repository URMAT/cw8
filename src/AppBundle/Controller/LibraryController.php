<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Reader;
use AppBundle\Form\RegistrationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class LibraryController extends Controller
{

    /**
     * @Route("/", name="mainPage")
     */
    public function indexAction()
    {

        $allCategories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();

        $allBooks = $this->getDoctrine()->getRepository('AppBundle:Book')->findAll();
        return $this->render('@App/Library/index.html.twig', array(
            'allBooks' => $allBooks,
            'categories' => $allCategories,
        ));
    }

    /**
     * @Route("/registration", name="addReader")
     */
    public function registrationReader(Request $request){
        $reader = new Reader();
        $ticketNumber = 'TN'.rand(1000, 99999);
        $reader->setNumberTicket($ticketNumber);
        $form = $this->createForm(RegistrationType::class, $reader);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $reader = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($reader);
            $em->flush();
            return $this->render('@App/Library/successful_registration.html.twig', array(
                'reader' => $reader
            ));
        }
        return $this->render('@App/Library/registration.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/takebook/{id}", requirements={"id": "\d+"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function takebookAction($id)
    {

        return $this->render('@App/Library/takebook.html.twig', array(

        ));
    }

    /**
     * @Route("/select/{id}", requirements={"id" = "\d+"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function selectGenre($id){

        $allCategories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();

            $selectedCategory = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Category')
                ->find($id);


            return $this->render('@App/Library/selectgenre.html.twig', array(
                'allBooks' => $selectedCategory->getBook(),
                'categories' => $allCategories
            ));
    }

    /**
     * @Route("/{_locale}/", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changeLanguageAction(Request $request){
        return $this->redirect(
            $request
                ->headers
                ->get('referer')
        );
    }
}
